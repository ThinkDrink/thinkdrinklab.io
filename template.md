<!DOCTYPE html>
<html lang="en">
    <head>
        <title>[Page title]</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial_scale=1">
        <style>
            body {
                font-family: Arial, Helvetica, sans-serif;
            }
            .header {
                padding: 80px; 
                text-align: center;
                background: #1abc9c; 
                color: white;
            }
            .header h1 {
                font-size: 40px;
            }
        </style>
    </head>
    <body>
        <div class="header">
            <h1>[Website Title]</h1>
            <p>[Introduction]</h1>
        </div>
        <div class="navbar">
            <a href="home.html">Home</a>
            <a href="about.html">About</a>
            <a href="buy.html">Buy</a>
            <a href="contact.html">Contact</a>
            <div class="Logo">
                <img src="Assets/Logo.jpg" width = "70px" height="30px" alt="Logo">
            </div>
        </div>
